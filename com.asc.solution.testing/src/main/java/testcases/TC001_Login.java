package testcases;

import java.util.Map;

import org.testng.annotations.Test;

import models.UserModel;
import pages.HeaderPage;
import pages.LoginPage;
import utils.GenericDataProvider;
import utils.UnitTestClassBase;
import utils.CSVAnnotation.CSVFileParameters;

public class TC001_Login extends UnitTestClassBase {

	HeaderPage headerPage;
	LoginPage loginPage;

	@Test(dataProvider = "dataproviderForTestCase", dataProviderClass = GenericDataProvider.class)
	@CSVFileParameters(path = "src\\main\\java\\testdata\\TD001_Login.csv", delimiter = ";")
	public void tc001_Login(int rowNo, Map<String, String> data) {
		UserModel userModel = new UserModel(data.get("username"), data.get("password"), data.get("displayname"));
		headerPage = new HeaderPage();
		loginPage = headerPage.openSignInModal();
		headerPage = loginPage.login(userModel);
		headerPage.verify_LoginSucceeded("Hi! " + userModel.getDisplayName());
	}	
}
