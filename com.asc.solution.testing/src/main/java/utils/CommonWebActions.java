package utils;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.aventstack.extentreports.Status;

public class CommonWebActions extends UnitTestClassBase {

	public CommonWebActions() {
		super();
	}

	public void click(WebElement element, String elementName) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			highlightElement(element);
			element.click();
			assertStep(Status.PASS, String.format("Clicked on %s successfully", elementName));
		} catch (Exception e) {
			assertStep(Status.FAIL, String.format("Failed to click on %s. ERROR: %s", elementName, e.getMessage()));
		}
	}

	public void click(By locator, String elementName) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			WebElement element = driver.findElement(locator);
			highlightElement(element);
			element.click();
			assertStep(Status.PASS, String.format("Clicked on %s successfully", elementName));
		} catch (Exception e) {
			assertStep(Status.FAIL, String.format("Failed to click on %s. ERROR: %s", elementName, e.getMessage()));
		}
	}

	public void sendKeys(WebElement element, String value, String elementName) {
		try {
			if (elementName.equalsIgnoreCase("password"))
				value = "********";
			wait.until(ExpectedConditions.visibilityOf(element));
			element.sendKeys(value);
			assertStep(Status.PASS, String.format("Input [%s] into the %s successfully", value, elementName));
		} catch (Exception e) {
			assertStep(Status.FAIL,
					String.format("Failed to input [%s] into the %s. ERROR: %s", value, elementName, e.getMessage()));
		}
	}

	public void isDisplayed(WebElement element, String elementName) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			element.isDisplayed();
			highlightElement(element);
			assertStep(Status.PASS, String.format("The %s is displayed as expected.", elementName));
		} catch (Exception e) {
			assertStep(Status.FAIL, String.format("The %s is NOT displayed which is NOT as expected", elementName));
		}
	}

	public void isTextDisplayed(WebElement element, String expectedText, String elementName) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			highlightElement(element);
			if (element.getText().trim().equalsIgnoreCase(expectedText.trim())) {
				assertStep(Status.PASS,
						String.format("Text [%s] of %s is displayed as expected", element.getText(), elementName));
			} else {
				assertStep(Status.FAIL, String.format("Compare Text is failed. Actual: [%s]. Expected: [%s]",
						element.getText(), expectedText));
			}
		} catch (Exception e) {
			assertStep(Status.FAIL,
					String.format("Unable to identify the %s . ERROR: %s", elementName, e.getMessage()));
		}
	}

	private static void highlightElement(WebElement element) {
		try {
			for (int i = 0; i < 3; i++) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"color: red; border: 4px solid red;");
				Thread.sleep(150);
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
				Thread.sleep(150);
			}
		} catch (Exception e) {
			System.out.println("Unable to highlight the web element");
		}
	}

	public void switchIframe(WebElement element) {
		driver.switchTo().frame(element);
	}

	public void deswitchIFrame() {
		driver.switchTo().defaultContent();
	}

}
