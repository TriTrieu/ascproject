package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

	public static Properties configProp;

	public static void loadConfigurationProperties() {
		try {
			configProp = new Properties();
			String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\config.properties";
			InputStream input = new FileInputStream(new File(filePath));
			configProp.load(input);
		} catch (Exception e) {
			System.out.println("Unable to load Config.properties file. ERROR: " + e.getMessage());
		}
		
	}

	public static String getPropertyValue(String key) {
		return configProp.getProperty(key);
	}

}
