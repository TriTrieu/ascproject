package utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class UnitTestClassBase {

	protected static WebDriver driver;
	protected static int TIMEOUT = 60;
	protected static WebDriverWait wait;

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest logger;

	@BeforeSuite
	public void beforeSuite() {
		PropertyLoader.loadConfigurationProperties();
	}

	@BeforeClass
	public void beforeClass() {

	}

	@BeforeTest
	public void startReport() throws UnknownHostException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\ExtentReport\\ExtentReport.html");
		// Create an object of Extent Reports
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Environment", PropertyLoader.getPropertyValue("sut"));
		extent.setSystemInfo("User Name", System.getProperty("user.name"));
		extent.setSystemInfo("Host Name", getComputerName());
		extent.setSystemInfo("OS", System.getProperty("os.name"));
		extent.setSystemInfo("Java Version", System.getProperty("java.version"));
		htmlReporter.loadXMLConfig(getExtentConfigFilePath());
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		launchBrowser();
		logger = extent.createTest(method.getName());
	}

	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - FAILDED", ExtentColor.RED));
			String screenshotPath = getScreenShot(driver, result.getName());
			logger.fail("Snapshot is below: " + logger.addScreenCaptureFromPath(screenshotPath));
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - SKIPPED", ExtentColor.ORANGE));
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			logger.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " - PASSED", ExtentColor.GREEN));
		}
		//driver.quit();

	}

	@AfterTest
	public void afterTest() {
		extent.flush();
	}

	@AfterClass
	public void afterClass() {

	}

	@AfterSuite
	public void afterSuite() {

	}

	private void launchBrowser() {
		String browserType = PropertyLoader.getPropertyValue("browserType");
		String url = PropertyLoader.getPropertyValue("url");
		if(browserType.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			ChromeOptions optionsChrome = new ChromeOptions();
			optionsChrome.addArguments("--start-maximized");
			optionsChrome.addArguments("disable-infobars");
			DesiredCapabilities capabilitiesChrome = new DesiredCapabilities();
			capabilitiesChrome.setCapability(ChromeOptions.CAPABILITY, optionsChrome);
			driver = new ChromeDriver(optionsChrome);
		}
		else if(browserType.equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver", getGeckoDriverPath());
			driver = new FirefoxDriver();
		}
		else {
			System.setProperty("webdriver.ie.driver", getIEDriverPath());
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
			options.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
			options.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, true);
			options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//			options.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			options.setCapability("requireWindowFocus", false);
			options.setCapability("enablePersistentHover", false);
			options.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "");
			options.addCommandSwitches("-private");
			driver = new InternetExplorerDriver(options);
		}

		driver.get(url);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, TIMEOUT);

	}

	private String getExtentConfigFilePath() {
		return System.getProperty("user.dir") + "\\src\\main\\resources\\extent-config.xml";
	}

	private String getGeckoDriverPath() {
		return System.getProperty("user.dir") + "\\src\\main\\resources\\geckodriver.exe";
	}

	private String getIEDriverPath() {
		return System.getProperty("user.dir") + "\\src\\main\\resources\\IEDriverServer.exe";
	}

	// This method is to capture the screenshot and return the path of the
	// screenshot.
	public static String getScreenShot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "\\ExtentReport\\" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

	public void assertStep(Status status, String details) {
		try {
			logger.log(status, details);
			if (status == Status.FAIL) {
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			System.out.println(String.format("ERROR occurred: %s", e.getMessage()));
		}
	}

	private static String getComputerName() {
		Map<String, String> env = System.getenv();
		if (env.containsKey("COMPUTERNAME"))
			return env.get("COMPUTERNAME");
		else if (env.containsKey("HOSTNAME"))
			return env.get("HOSTNAME");
		else
			return "Unknown Computer";
	}
}
