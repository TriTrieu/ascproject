package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import models.UserModel;


public class LoginPage extends MasterPage{

	public LoginPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.CSS, using = "input[name='sloginname']")
	private WebElement textboxUserName;
	
	@FindBy(how = How.CSS, using = "input[name='spassword']")
	private WebElement textboxPassword;
	
	@FindBy(how = How.CSS, using = "a#btnValid")
	private WebElement buttonSignIn;
	

	public HeaderPage login(UserModel userModel) {
		sendKeys(textboxUserName, userModel.getUserName(), "[User Name] textbox");
		sendKeys(textboxPassword, userModel.getPassword(), "[Password] textbox");
		click(buttonSignIn, "[Sign In] button");
		return new HeaderPage();
	}
	
	
}
