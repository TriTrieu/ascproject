package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage extends MasterPage {

	public HeaderPage() {
		super();
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CSS, using = "ul#loginMenu a[title='Sign In']")
	private WebElement buttonSignIn;
	
	@FindBy(how = How.CSS, using = "a#dropdownMenuAccount span.name")
	private WebElement labelAccountName;

	public LoginPage openSignInModal() {
		click(buttonSignIn, "[Sign In] button");
		return new LoginPage();
	}

	public void verify_LoginSucceeded(String displayName) {
		isTextDisplayed(labelAccountName, displayName, "[Display Name] label");

	}
}
