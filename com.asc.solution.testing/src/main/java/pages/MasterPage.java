package pages;

import org.openqa.selenium.support.PageFactory;

import utils.CommonWebActions;

public class MasterPage extends CommonWebActions{

	public MasterPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
