package models;

public class UserModel {

	private String userName;
	private String password;
	private String displayName;

	

	public UserModel(String userName, String password, String displayName) {
		super();
		this.userName = userName;
		this.password = password;
		this.displayName = displayName;
	}

	public UserModel() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
